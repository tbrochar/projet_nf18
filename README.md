Sujet : Le Musée du Louvre

Le musée du Louvre a besoin d'un système de gestion de ses œuvres et expositions. Nous sommes en charge de la modélisation et de l'implémentation d'une base de données permettant de répondre au problème, et développererons une interface permettant l'interaction avec ce système.

Membres du groupe : 
* Aness KEBIR
* Pierrick GOUJET
* Théo BROCHARD
